const {createNewUser}=require('../service/userServices');
const { OK } = require('http-status-codes');
const bcrypt = require('bcrypt');
const user = require('../models/user');

const registerUser = async (req, res) => {
	bcrypt.hash(req.body.password, 10, (err, hash) =>{
		if(err){
			return res.status(500).json({
				error: err
			});
		}else{
			 createNewUser({
				name: req.body.name,
				email: req.body.email,
				mobilenumber: req.body.mobilenumber,
				password: hash
			});
		}
	})
    try {
         res.status(OK).json({ message: `User created successfully` });
	} catch (error) {
		console.log(error);
	}
};
const loginUser = async (req,res) => {
     user.findOne({email: req.body.email})
	 .then(user => {
		 if(!user) res.status(404).json({error: 'no user with that email found'})
		 else{
			 bcrypt.compare(req.body.password, user.password, (error,match)=>{
				 if(error) res.status(500).json(error)
				 else if (match) res.status(200). json({message: 'login successfully'})
				 else res.status(403). json({error: 'password not match'})
			 })
		 }
	 })
	 .catch(error => {
		 res.status(500). json(error)
	 })
};
const logoutUser = async (req,res) =>{
 res.status(200). json({message: 'logout successfully'})
}
module.exports= {
    registerUser,loginUser,logoutUser
}