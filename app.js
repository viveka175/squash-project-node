const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const route = require('./routers')
const port  = 8081;

const express = require('express');
const app = express();

const cors = require('./cors');
app.use(cors.allowCrossDomain);

mongoose.connect('mongodb://localhost:27017/studentsdata',{useNewUrlParser:true})
const db = mongoose.connection;

db.on("error",()=>{console.log("error in conection");})
db.once('open',()=>{console.log("Connected");})

// app.set('view engine','ejs')

// app.use(express.static('public'))

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(route);

app.listen(port);
console.log(port);