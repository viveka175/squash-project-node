const express = require('express');
const router = express.Router();
const userService = require('../routeService/userService');

router.post('/register', userService.registerUser);

router.post('/login',userService.loginUser)

router.post('/logout',userService.logoutUser)

module.exports = router;


