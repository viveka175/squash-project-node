'use strict';
const express = require('express');
const route = express.Router();
const { OK } = require('http-status-codes');
const userRoutes= require('./userRoutes')

// User specific API's
route.use('/user', userRoutes);

module.exports = route;