
const User = require('../models/user');

module.exports = {

    createNewUser: async (userDetails) => {
        try {
            return await new User(userDetails).save();
        } catch (error) {
            throw error;
        }
    }
}